from django.urls import path, include
from django.contrib.auth import views
from .views import index, books, logout_auth, add_fav, del_fav

app_name = 'list_buku'

urlpatterns = [
    path('', index, name='list_bukus'),
    path('books', books, name='books'),
    path('login', views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout-auth', logout_auth, name='logout-auth'),
    path('add_fav', add_fav, name='add_fav'),
    path('del_fav', del_fav, name='del_fav'),
]