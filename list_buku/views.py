from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .forms import SearchForm
import json
import requests

response = {}

def index(request):
    form = SearchForm
    response['form'] = form
    if request.user.is_authenticated:
        set_user_session(response, request)
    return render(request, 'daftar_buku.html', response)

def books(request):
    url = 'https://www.googleapis.com/books/v1/volumes?q='
    if 'search' in request.GET:
        url += request.GET['search']
    else:
        url += 'quilting'
    api = requests.get(url)
    books = api.json()
    for book in books['items']:
        book['volumeInfo']['star'] = 0
        if book['id'] in request.session['ch_book']:
            book['volumeInfo']['star'] = 1
    return JsonResponse(books)

def logout_auth(request):
    request.session.flush()
    return HttpResponseRedirect('/')

def set_user_session(response, request):
    if 'fav' not in request.session.keys():
        request.session['fav'] = 0
        request.session['ch_book'] = []
    response['fav'] = request.session['fav']
    response['ch_book'] = request.session['ch_book']

def add_fav(request):
    response = {}
    request.session['fav'] += 1;
    request.session['ch_book'].append(request.GET.get('id_buku'))
    response['fav'] = request.session['fav']
    return JsonResponse(response)

def del_fav(request):
    response = {}
    request.session['fav'] -= 1;
    request.session['ch_book'].remove(request.GET.get('id_buku'))
    response['fav'] = request.session['fav']
    return JsonResponse(response)