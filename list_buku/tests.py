from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, books

# Create your tests here.
class ListBukuUnitTest(TestCase):

    def test_list_buku_is_exist(self):
        response = Client().get('/list_buku/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/list_buku/')
        self.assertEqual(found.func, index)

    def test_using_list_buku_template(self):
        response = Client().get('/list_buku/')
        self.assertTemplateUsed(response, 'daftar_buku.html')

    def test_using_books_func(self):
        found = resolve('/list_buku/books')
        self.assertEqual(found.func, books)
