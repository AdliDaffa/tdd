from django import forms

class SearchForm(forms.Form):
    keys = forms.CharField(label='Search', max_length=255, required=True, widget=forms.TextInput())