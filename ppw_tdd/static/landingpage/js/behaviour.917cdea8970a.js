function changeTheme() {
    var now = document.getElementById('changetheme');
    var header = document.getElementById('header');
    var body = document.getElementsByTagName('body')[0];
    var cards = document.getElementsByClassName('card-header');
    if(now.style.backgroundColor == 'rgb(75, 209, 230)') {
        now.style.backgroundColor = 'green';
        now.style.color = 'white';
        header.style.backgroundColor = '#4bd1e6';
        header.style.color = 'black';
        body.style.backgroundColor = 'white';
        for(var i = 0; i < cards.length; ++i) {
            cards[i].style.backgroundColor = '#4bd1e6';
            cards[i].style.color = 'black';
        }
    } else {
        now.style.backgroundColor = '#4bd1e6';
        now.style.color = 'black';
        header.style.backgroundColor = 'green';
        header.style.color = 'white';
        body.style.backgroundColor = '#e9ecef';
        for(var i = 0; i < cards.length; ++i) {
            cards[i].style.backgroundColor = 'green';
            cards[i].style.color = 'white';
        }
    }
}

$( function() {
    $( "#accordion" ).accordion({'collapsible': true,
        'active': false,
        'heightStyle': 'content'});
} );

$(window).load(function() {
    $(".loader").fadeOut(4000);
});