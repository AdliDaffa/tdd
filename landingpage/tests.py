from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .forms import StatusForm
from .models import PostStatus
from .views import index, status_list, profil_index
import time
import ast

# Create your tests here.
class LandingPageUnitTest(TestCase):

    def test_landing_page_is_exist(self):
        response = Client().get('/landingpage/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/landingpage/')
        self.assertEqual(found.func, index)

    def test_using_landing_page_template(self):
        response = Client().get('/landingpage/')
        self.assertTemplateUsed(response, 'landingpage.html')

    def test_lading_page_content_is_written(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', html_response)

    def test_root_url_redirect_to_landing_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/list_buku/', 302, 200)

    def test_model_for_posting_status(self):
        new_post = PostStatus.objects.create(datetime=datetime.now(),status="lagi koding pepew")
        count_post = PostStatus.objects.all().count()
        self.assertEqual(count_post, 1)

    def test_can_save_POST_request(self):
        response = Client().post('/landingpage/post_status', {'status': 'begadang ngoding pepew'})
        
        self.assertEqual(response.status_code, 302)

        new_response = Client().get('/landingpage/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('begadang ngoding pepew', html_response)

    def test_failed_to_save_POST_request(self):
        bug = "bug" * 1000
        response = Client().post('/landingpage/post_status', {'status': bug})
        new_response = Client().get('/landingpage/')
        html_response = new_response.content.decode('utf8')
        self.assertNotIn(bug, html_response)

    def test_form_validation_for_blank_input(self):
        form = StatusForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ['This field is required.']
            )


class ProfilPageUnitTest(TestCase):

    def test_profil_is_exist(self):
        response = Client().get('/landingpage/profil')
        self.assertEqual(response.status_code, 200)

    def test_using_profil_func(self):
        found = resolve('/landingpage/profil')
        self.assertEqual(found.func, profil_index)

    def test_using_profil_template(self):
        response = Client().get('/landingpage/profil')
        self.assertTemplateUsed(response, 'profil.html')

    def test_about_me_content_is_written(self):
        request = HttpRequest()
        response = profil_index(request)
        desc = "Saya seorang Mahasiswa Fakultas Ilmu Komputer dari Universitas Indonesia. Saya Merupakan programmer web junior. Ini merupakan website pertama saya. Enjoy it:)"
        html_response = response.content.decode('utf8')
        self.assertIn(desc, html_response)

class SubcribesMeUnitTest(TestCase):

    def test_subscribe_me_is_exist(self):
        response = Client().get('/landingpage/profil#form')
        self.assertEqual(response.status_code, 200)

    def test_can_check_email(self):
        email = 'email@email.email'
        json_response = Client().post('/landingpage/email-checker', {'email': email})
        self.assertEqual(json_response.status_code, 200)

    def test_can_subscribe_and_cannot_register_again_using_same_email(self):
        nama = 'tes'
        email = 'tes@tes.tes'
        password = 'testestes'
        json_response = Client().post('/landingpage/subscribe', {'nama':nama, 'email':email, 'password':password})
        self.assertEqual(json_response.status_code, 200)
        new_response = Client().post('/landingpage/email-checker', {'email': email})
        self.assertEqual(new_response.status_code, 200)



# class LandingPageFunctionalTest(TestCase):

#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')        
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path= './chromedriver.log'
#         service_args= ['--verbose']
#         self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.browser.implicitly_wait(25)
#         super(LandingPageFunctionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(LandingPageFunctionalTest, self).tearDown()

#     def test_html_positioning(self):
#         # user membuka web saya dihalaman profil
#         self.browser.get('http://addffasilkom-tdd.herokuapp.com/landingpage/profil')
#         time.sleep(3)

#         # user nanti akan melihat foto saya di profil saya
#         image = self.browser.find_element_by_tag_name('img')
#         # cek apakah tag image ada
#         self.assertNotEqual(image, None)
#         # cek apakah gambar dari image tersebut adalah source foto Saya
#         my_img = 'http://ppw-b-addffasilkom.herokuapp.com/static/profil/image.jpg'
#         self.assertEqual(my_img, str(image.get_attribute('src')))

#         # lalu user dapat melihat nama saya dengan jelas
#         name = self.browser.find_element_by_tag_name('h1')
#         # cek apakah tag h1 ada
#         self.assertNotEqual(name, None)
#         # cek apakah user melihat nama saya dengan benar
#         my_name = 'Mohammad Adli'
#         self.assertEqual(my_name, name.text)

#     def test_css_styling(self):
#         # user membuka web halaman profil saya
#         self.browser.get('http://addffasilkom-tdd.herokuapp.com/landingpage/profil')
#         time.sleep(3)

#         # user akan melihat warna biru langit pada bagian navbar web saya
#         navbar = self.browser.find_element_by_class_name('jumbotron')
#         # cek apakah warna sudah sesuai atau tidak
#         color = 'rgba(75, 209, 230, 1)'
#         self.assertEqual(color, navbar.value_of_css_property('background-color'))

#         # lalu user dapat melihat deskripsi singkat tentang saya
#         desc = self.browser.find_element_by_tag_name('p')
#         # cek apakah deskripsi tersebut terletak di center (tengah) atau tidak
#         self.assertEqual('center', desc.value_of_css_property('text-align'))