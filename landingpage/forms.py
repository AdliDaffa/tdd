from django import forms

class StatusForm(forms.Form):
    attrs = {
        'class': 'form-control',
        'rows': '5',
        'placeholder': 'Status Anda',
    }
    status = forms.CharField(label='', max_length=300, widget=forms.Textarea(attrs=attrs), required=True)

class SubscribeForm(forms.Form):
    attrs = {
        'class': 'form-control',
    }
    nama = forms.CharField(label='Nama', max_length=50, required=True, widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(label='Email', max_length=50, required=True, widget=forms.EmailInput(attrs=attrs))
    password = forms.CharField(label='Password', max_length=50, required=True, widget=forms.PasswordInput(attrs=attrs))