from django.urls import path
from .views import index, post_status, profil_index, check_email, subscribe, list_subscribers

app_name = 'landingpage'

urlpatterns = [
    path('', index, name='landingpage'),
    path('post_status', post_status, name='post_status'),
    path('profil', profil_index, name='profil'),
    path('email-checker', check_email, name='check_email'),
    path('subscribe', subscribe, name='subscribe'),
    path('list-subscribers', list_subscribers, name='list_subscribers'),
]
