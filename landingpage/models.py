from django.db import models
from datetime import datetime

# Create your models here.
class PostStatus(models.Model):
    datetime = models.DateTimeField(default=datetime.now())
    status = models.CharField(max_length=50)

class Subscriber(models.Model):
    nama = models.CharField(max_length=50)
    email = models.EmailField(unique=True, max_length=50)
    password = models.CharField(max_length=50)