from django.contrib import admin
from .models import PostStatus, Subscriber

# Register your models here.
admin.site.register(PostStatus)
admin.site.register(Subscriber)