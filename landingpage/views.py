from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.http import HttpResponse
from .forms import StatusForm, SubscribeForm
from .models import PostStatus, Subscriber

# Create your views here.
status_list = PostStatus.objects.all()

def index(request):
    response = {'status_form': StatusForm, 'status_list': status_list[::-1]}
    return render(request, 'landingpage.html', response)

def post_status(request):
    form = StatusForm(request.POST or None)
    response = {}
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        new_status = PostStatus(status=response['status'])
        new_status.save()
        return HttpResponseRedirect('/landingpage')
    else:
        return HttpResponseRedirect('/landingpage')

def profil_index(request):
    response = {'form' : SubscribeForm}
    return render(request, 'profil.html', response)

def check_email(request):
    email = request.POST['email']
    db = Subscriber.objects.all()
    err = 'Email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain'
    for data in db:
        if email == data.email:
            return JsonResponse({'message': err})
    return JsonResponse({'message': 'Ok'})

def subscribe(request):
    nama = request.POST['nama']
    email = request.POST['email']
    password = request.POST['password']
    Subscriber(nama=nama, email=email, password=password).save()
    return JsonResponse({'message': 'Data anda berhasil disimpan'})

def list_subscribers(request):
    subscribersjson = list()
    for i in Subscriber.objects.all():
        subscribersjson.append(i.nama)
    return JsonResponse({'subscribers' : subscribersjson})